<?php
/**
 * Template file for jqzoom element
 *
 * Variables:
 *
 *  $element - the whole element
 *
 *  $rendered
 *  The renderd jqzoom image area, or remove it, but care to
 *  drupal_render($element['#element']['items']);
 *
 *  $title The field title
 */
?>
<div class="symp-zoom-element">
  <?php print $rendered ?>
</div>
