(function ($) {

  Drupal.behaviors.symp_zoom = {
    attach: function (context, settings) {
      $(".symp-zoom-element #symp-zoom-main", context).elevateZoom({
        gallery: 'symp-zoom-gallery',
        zoomType: "inner",
        cursor: "crosshair",
        galleryActiveClass: 'active',
        imageCrossfade: true,
        responsive: false,
        scrollZoom : true,
        loadingIcon: '/misc/throbber.gif'
      });
    }
  };

})(jQuery);